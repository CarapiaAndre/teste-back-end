IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Imoveis] (
    [Id] int NOT NULL IDENTITY,
    [EmailProprietario] nvarchar(320) NOT NULL,
    [Rua] nvarchar(100) NOT NULL,
    [Numero] nvarchar(10) NULL,
    [Complemento] nvarchar(20) NULL,
    [Bairro] nvarchar(100) NOT NULL,
    [Cidade] nvarchar(100) NOT NULL,
    [Estado] nvarchar(30) NOT NULL,
    CONSTRAINT [PK_Imoveis] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [TipoPessoas] (
    [Id] int NOT NULL IDENTITY,
    [Descricao] nvarchar(20) NOT NULL,
    CONSTRAINT [PK_TipoPessoas] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Contratos] (
    [Id] int NOT NULL IDENTITY,
    [Documento] nvarchar(18) NOT NULL,
    [EmailContratante] nvarchar(320) NOT NULL,
    [NomeContratante] nvarchar(256) NOT NULL,
    [DtContrato] datetime2 NOT NULL,
    [TipoPessoaId] int NOT NULL,
    [ImovelId] int NOT NULL,
    CONSTRAINT [PK_Contratos] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Contratos_Imoveis_ImovelId] FOREIGN KEY ([ImovelId]) REFERENCES [Imoveis] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Contratos_TipoPessoas_TipoPessoaId] FOREIGN KEY ([TipoPessoaId]) REFERENCES [TipoPessoas] ([Id]) ON DELETE CASCADE
);

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Descricao') AND [object_id] = OBJECT_ID(N'[TipoPessoas]'))
    SET IDENTITY_INSERT [TipoPessoas] ON;
INSERT INTO [TipoPessoas] ([Id], [Descricao])
VALUES (1, N'Física');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Descricao') AND [object_id] = OBJECT_ID(N'[TipoPessoas]'))
    SET IDENTITY_INSERT [TipoPessoas] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Descricao') AND [object_id] = OBJECT_ID(N'[TipoPessoas]'))
    SET IDENTITY_INSERT [TipoPessoas] ON;
INSERT INTO [TipoPessoas] ([Id], [Descricao])
VALUES (2, N'Jurídica');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Descricao') AND [object_id] = OBJECT_ID(N'[TipoPessoas]'))
    SET IDENTITY_INSERT [TipoPessoas] OFF;

GO

CREATE UNIQUE INDEX [IX_Contratos_ImovelId] ON [Contratos] ([ImovelId]);

GO

CREATE INDEX [IX_Contratos_TipoPessoaId] ON [Contratos] ([TipoPessoaId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201019092342_initial', N'3.1.9');

GO

