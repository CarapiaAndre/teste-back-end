﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContratosController : ControllerBase
    {
        private readonly WebAPIContext _context;

        public ContratosController(WebAPIContext context)
        {
            _context = context;
        }

        // GET: api/Contratos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Contrato>>> GetContrato()
        {
            return await _context.Contratos.AsNoTracking().Include(i => i.Imovel).ToListAsync();
        }

        // GET: api/Contratos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Contrato>> GetContrato(int id)
        {
            var contrato = await _context.Contratos.AsNoTracking()
                .Include(i => i.Imovel)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (contrato == null)
            {
                return NotFound();
            }

            return contrato;
        }

        [HttpGet]
        [Route("/api/Contratos/Lista")]
        public async Task<ActionResult<IEnumerable<ContratoLista>>> GetContratoLista()
        {
            return await _context.Contratos.
                AsNoTracking().
                Include(i => i.Imovel)
                .Select(c => new ContratoLista
                {
                    ImovelId = c.ImovelId,
                    EmailProprietario = c.Imovel.EmailProprietario,
                    NomeContratante = c.NomeContratante,
                    EmailContratante = c.EmailContratante
                })
                .ToListAsync();
        }

        // GET: /api/Contratos/Lista/5
        [HttpGet("/api/Contratos/Lista/{id}")]
        public async Task<ActionResult<ContratoLista>> GetContratoLista(int id)
        {
            var contrato = await _context.Contratos.AsNoTracking()
                .Include(i => i.Imovel)
                .Where(x => x.Id == id)
                .Select(c => new ContratoLista
                {
                    ImovelId = c.ImovelId,
                    EmailProprietario = c.Imovel.EmailProprietario,
                    NomeContratante = c.NomeContratante,
                    EmailContratante = c.EmailContratante
                })
                .FirstOrDefaultAsync();

            if (contrato == null)
            {
                return NotFound();
            }

            return contrato;
        }

        // PUT: api/Contratos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutContrato(int id, Contrato contrato)
        {
            if (id != contrato.Id)
            {
                return BadRequest();
            }

            _context.Entry(contrato).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContratoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Contratos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Contrato>> PostContrato(Contrato contrato)
        {
            if (!contrato.DocumentoValido())
            {
                throw new ArgumentException("Documento inválido");
            }

            contrato.Documento = contrato.Documento.Replace(".", "").Replace("-", "").Replace("/", "");

            _context.Contratos.Add(contrato);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                throw new ArgumentException("Essa propriedade já está associada a um contrato");

            }

            return CreatedAtAction("GetContrato", new { id = contrato.Id }, contrato);
        }

        // DELETE: api/Contratos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Contrato>> DeleteContrato(int id)
        {
            var contrato = await _context.Contratos.FindAsync(id);
            if (contrato == null)
            {
                return NotFound();
            }

            _context.Contratos.Remove(contrato);
            await _context.SaveChangesAsync();

            return contrato;
        }

        private bool ContratoExists(int id)
        {
            return _context.Contratos.Any(e => e.Id == id);
        }
    }
}
