﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Imoveis] (
    [Id] int NOT NULL IDENTITY,
    [EmailProprietario] nvarchar(max) NOT NULL,
    [Rua] nvarchar(max) NOT NULL,
    [Numero] nvarchar(max) NULL,
    [Complemento] nvarchar(max) NULL,
    [Bairro] nvarchar(max) NOT NULL,
    [Cidade] nvarchar(max) NOT NULL,
    [Estado] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Imoveis] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Contratos] (
    [Id] int NOT NULL IDENTITY,
    [TipoPessoa] nvarchar(max) NOT NULL,
    [Documento] nvarchar(max) NOT NULL,
    [EmailContratante] nvarchar(max) NOT NULL,
    [NomeContratante] nvarchar(max) NOT NULL,
    [DtContrato] datetime2 NOT NULL,
    [ImovelId] int NOT NULL,
    CONSTRAINT [PK_Contratos] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Contratos_Imoveis_ImovelId] FOREIGN KEY ([ImovelId]) REFERENCES [Imoveis] ([Id]) ON DELETE CASCADE
);

GO

CREATE UNIQUE INDEX [IX_Contratos_ImovelId] ON [Contratos] ([ImovelId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201016104358_Initial', N'3.1.9');

GO

