﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class WebAPIContext : DbContext
    {
        public WebAPIContext(DbContextOptions<WebAPIContext> options)
            : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=ImoveisDB;Data Source=DESKTOP-KD32Q5K\\SQLEXPRESS");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Imovel>()
                .HasOne(i => i.Contrato)
                .WithOne(c => c.Imovel)
                .HasForeignKey<Contrato>(c => c.ImovelId);

            modelBuilder.Entity<TipoPessoa>().HasData(
                new TipoPessoa
                {
                    Id = 1,
                    Descricao = "Física"
                }
            );
            modelBuilder.Entity<TipoPessoa>().HasData(
                new TipoPessoa
                {
                    Id = 2,
                    Descricao = "Jurídica"
                }
            );
        }

        public DbSet<Imovel> Imoveis { get; set; }

        public DbSet<Contrato> Contratos { get; set; }

        public DbSet<TipoPessoa> TipoPessoas { get; set; }
    }
}
