﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Contrato
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Documento é obrigatório")]
        [MaxLength(18, ErrorMessage = "Este campo deve possuir no máximo 14 caracteres")]
        [MinLength(11, ErrorMessage = "Este campo deve possuir no mínimo 11 caracteres")]
        [RegularExpression(
            @"([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})",
            ErrorMessage = "CPF ou CNPJ inválido"
        )]
        public string Documento { get; set; }

        [Required(ErrorMessage = "E-mail é obrigatório")]
        [MaxLength(320, ErrorMessage = "Este campo deve possuir no máximo 320 caracteres")]
        [MinLength(5, ErrorMessage = "Este campo deve possuir no mínimo 5 caracteres")]
        public string EmailContratante { get; set; }

        [Required(ErrorMessage = "Nome Completo é obrigatório")]
        [MaxLength(256, ErrorMessage = "Este campo deve possuir no máximo 256 caracteres")]
        [MinLength(5, ErrorMessage = "Este campo deve possuir no mínimo 5 caracteres")]
        public string NomeContratante { get; set; }

        [Required(ErrorMessage = "Data do Contrato é obrigatório")]
        public DateTime DtContrato { get; set; }

        [Required(ErrorMessage = "Tipo de Pessoa é obrigatório")]
        public int TipoPessoaId { get; set; }
        public TipoPessoa TipoPessoa { get; set; }

        public int ImovelId { get; set; }
        public Imovel Imovel { get; set; }

        public bool DocumentoValido()
        {
            if (TipoPessoaId == 1)
            {
                var cpfRegex = new Regex(@"^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2})|([0-9]{11}))$");
                if (!cpfRegex.IsMatch(Documento))
                {
                    return false;
                }
            }
            if (TipoPessoaId == 2)
            {
                var cnpjRegex = new Regex(@"[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2}");
                if (!cnpjRegex.IsMatch(Documento))
                {
                    return false;
                }
            }

            return true;
        }

    }
}
