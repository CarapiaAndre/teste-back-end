﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class TipoPessoa
    {   
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage ="Descrição é obrigatório")]
        [MaxLength(20, ErrorMessage ="Este campo deve possuir no máximo 20 caracteres")]
        public string Descricao { get; set; }
    }
}
