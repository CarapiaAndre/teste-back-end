﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class ContratoLista
    {
        public int ImovelId { get; set; }
        public string EmailProprietario { get; set; }
        public string NomeContratante { get; set; }
        public string EmailContratante { get; set; }
    }
}
