﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Imovel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Email do Proprietário é obrigatório")]
        [MaxLength(320, ErrorMessage = "Este campo deve possuir no máximo 320 caracteres")]
        [MinLength(5, ErrorMessage = "Este campo deve possuir no mínimo 5 caracteres")]
        public string EmailProprietario { get; set; }

        [Required(ErrorMessage = "Rua é obrigatório")]
        [MaxLength(100, ErrorMessage = "Este campo deve possuir no máximo 100 caracteres")]
        public string Rua { get; set; }

        [MaxLength(10, ErrorMessage = "Este campo deve possuir no máximo 10 caracteres")]
        public string Numero { get; set; }

        [MaxLength(20, ErrorMessage = "Este campo deve possuir no máximo 20 caracteres")]
        public string Complemento { get; set; }

        [Required(ErrorMessage = "Bairro é obrigatório")]
        [MaxLength(100, ErrorMessage = "Este campo deve possuir no máximo 100 caracteres")]
        public string Bairro { get; set; }

        [Required(ErrorMessage = "Cidade é obrigatório")]
        [MaxLength(100, ErrorMessage = "Este campo deve possuir no máximo 100 caracteres")]
        public string Cidade { get; set; }

        [Required(ErrorMessage = "Estado é obrigatório")]
        [MaxLength(30, ErrorMessage = "Este campo deve possuir no máximo 30 caracteres")]
        public string Estado { get; set; }

        public Contrato Contrato { get; set; }
    }
}
