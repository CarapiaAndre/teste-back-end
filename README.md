# Teste Back End .Net Core C#
## Subir WebAPI Local
Na raiz desse repositório foi incluído um arquivo compactado (Release WebAPI Imoveis.zip). Basta descompactar o arquivo e executar **WebAPI.exe**
*Porta padrão: 5000*

## Script de criação do Banco de Dados
É possível encontrar o script para geração das tabelas na raiz desse repositório (Arquivo: Script DataBase SQL.sql)

## Observações
Para facilitar a depuração e testes da API, foi gerado duas branchs:
- **Master**: Realiza conexão com o SQL Server como banco de dados
- **MemoryDatabase**: O arquivo de realease foi gerado a partir dessa versão, utilizando banco em memória para facilitar a publicação localmente com fins de testes

## Documentação Funcionalidades
### Funcionalidade 1
#### Cadastro de Imóvel
- Verbo: POST
- EndPoint: api/imoveis
- Request Body:
    ~~~
    {
        "emailProprietario": "string | obrigatório",
        "rua": "string | obrigatório",
        "numero": "string | opcional",
        "complemento": "string | opcional",
        "bairro": "string | obrigatório",
        "cidade": "string | obrigatório",
        "estado": "string | obrigatório"
    }
    ~~~
### Funcionalidade 2
#### Visualização Imóveis
- Verbo: GET
- EndPoint: api/imoveis | api/imoveis/ìd

### Funcionalidade 3
#### Remoção de Imóvel
- Verbo: DELETE
- EndPoint: api/imoveis/ìd

### Funcionalidade 4
#### Atualização de Imóvel
- Verbo: PUT
- EndPoint: api/imoveis/ìd

### Funcionalidade 5
#### Cadastro de Contrato
- Verbo: POST
- EndPoint: api/contratos
- Request Body:
    ~~~
    {
        "idImovel": "string | obrigatório",
        "tipoPessoaId": "int | obrigatório | 1 para pessoa física / 2 para pessoa jurídica",
        "documento": "string | obrigatório | CPF para TipoPessoaId = 1 / CNPJ para TipoPessoaId = 2",
        "emailContratante": "string | obrigatório",
        "nomeContratante": "string | obrigatório",
        "dtContrato": "datetime | obrigatório"
    }
    ~~~

### Funcionalidade 6
#### Visualização Contratos
- Verbo: GET
- EndPoint: api/contratos/lista | api/contratos/lista/ìd
- Obs: Endpoint para obter listagem com campos filtrados ( E-mail do Proprietário, Nome completo do contratante, E-mail do Contratante;)

### Funcionalidade 6 / Adicional
#### Visualização Contratos
- Verbo: GET
- EndPoint: api/contratos | api/contratos/ìd
- Obs: Endpoint para obter listagem com todos os campos da tabela
